package dam.androidmiguelangel.app_tasca2;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

public class FragmentDetalle extends Fragment {

    private TextView Titol;
    private ImageView imageDiscDetail;
    private TextView descripcion;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detalle, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Disc disco;

        if (!esTablet(getActivity())) {
            Intent intent = getActivity().getIntent();
            disco = (Disc) intent.getSerializableExtra("Disco");
        } else {
            disco = (Disc) getArguments().getSerializable("Disco");
        }

        Titol = getView().findViewById(R.id.Titol);
        Titol.setText(disco.getTitol());
        imageDiscDetail = getView().findViewById(R.id.imageDiscDetail);
        imageDiscDetail.setImageResource(disco.getImage());
        descripcion = getView().findViewById(R.id.descripcion);
        descripcion.setText(disco.getDescripcion());

    }

    public static boolean esTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

}