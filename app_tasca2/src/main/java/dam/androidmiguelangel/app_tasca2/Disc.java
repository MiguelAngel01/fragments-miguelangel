package dam.androidmiguelangel.app_tasca2;

import android.media.Image;
import android.widget.ImageView;

import java.io.Serializable;

public class Disc implements Serializable {

    private String titol;
    private String descripcion;
    private int imageID;

    public Disc(String titol, String descripcion, int imageID) {
        this.titol = titol;
        this.descripcion = descripcion;
        this.imageID = imageID;
    }

    public Disc(String titol, String descripcion) {
        this.titol = titol;
        this.descripcion = descripcion;
    }

    public String getTitol() {
        return titol;
    }

    public void setTitol(String titol) {
        this.titol = titol;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getImage() {
        return imageID;
    }

    public void setImage(int imageID) {
        this.imageID = imageID;
    }
}
