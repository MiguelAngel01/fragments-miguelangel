package dam.androidmiguelangel.app_tasca2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.Serializable;

public class FragmentListado extends Fragment {

    private Disc[] discos =
            new Disc[]{
                    new Disc("Mago de Oz", "Primer álbum de Mago de Öz - 1994", R.drawable.magodeoz),
                    new Disc("Jesús de Chamberí", "Segundo álbum de Mago de Öz - 1996", R.drawable.jesus),
                    new Disc("La leyenda de La Mancha", "Tercer álbum de Mago de Öz - 1998 - Adaptación musical del libro del Quijote", R.drawable.lamancha),
                    new Disc("Finisterra", "Cuarto álbum de Mago de Öz - 2000", R.drawable.finisterra),
                    new Disc("Gaia", "Quinto álbum de Mago de Öz - 2003", R.drawable.gaia),
                    new Disc("Belfast", "Sexto álbum de Mago de Öz - 2004", R.drawable.belfast),
                    new Disc("Gaia II: La voz dormida", "Septimo álbum de Mago de Öz - 2005 - Es su álbum más vendido", R.drawable.gaia2),
                    new Disc("La ciudad de los árboles", "Octavo álbum de Mago de Öz - 2007", R.drawable.arboles),
                    new Disc("Gaia III: Atlantia", "Noveno álbum de Mago de Öz - 2010", R.drawable.gaia3),
                    new Disc("Gaia: epílogo", "Decimo álbum de Mago de Öz - 2010 - Último álbum en el que el cantante es Jose Andrea", R.drawable.gaiaepilogo),
                    new Disc("Hechizos, pócimas y brujería", "Undecimo álbum de Mago de Öz - 2012 - Primer álbum en el que el cantante es Zeta", R.drawable.hechizos),
                    new Disc("Ilussia", "Duodecimo álbum de Mago de Öz - 2014", R.drawable.ilussia),
                    new Disc("Finisterra Opera Rock", "Decimotercero álbum de Mago de Öz - 2015 - Remake del álbum Finisterra del 2000", R.drawable.finisterra2015),
                    new Disc("Ira Dei", "Decimocuarto álbum de Mago de Öz - 2019", R.drawable.iradei),
            };

    private ListView lstListado;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_listado, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        lstListado = getView().findViewById(R.id.lstListado);
        lstListado.setAdapter(new AdaptadorDiscos(this));

        lstListado.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (esTablet(getActivity())) {
                    FragmentDetalle fragmentDetalle = new FragmentDetalle();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("Disco", discos[position]);
                    fragmentDetalle.setArguments(bundle);
                    FragmentManager FM = getActivity().getSupportFragmentManager();
                    FragmentTransaction FT = FM.beginTransaction();
                    FT.replace(R.id.fragmentContainer, fragmentDetalle);
                    FT.commit();
                } else {
                    Intent intent = new Intent(getActivity(), ActivityDetalle.class);
                    intent.putExtra("Disco", discos[position]);
                    getActivity().startActivity(intent);
                }
            }
        });

    }

    public static boolean esTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    class AdaptadorDiscos extends ArrayAdapter<Disc> {
        Activity context;

        AdaptadorDiscos(Fragment context) {
            super(context.getActivity(), R.layout.constraint, discos);
            this.context = context.getActivity();
        }

        public View getView(int position, View converView, ViewGroup parent) {
            LayoutInflater inflater = context.getLayoutInflater();
            View item = inflater.inflate(R.layout.constraint, null);

            ImageView ivAlbum = item.findViewById(R.id.ivAlbum);
            ivAlbum.setImageResource(discos[position].getImage());

            TextView tvTitol = item.findViewById(R.id.tvTitol);
            tvTitol.setText(discos[position].getTitol());

            TextView tvDescripcion = item.findViewById(R.id.tvDescripcion);
            tvDescripcion.setText(discos[position].getDescripcion());

            return item;
        }

    }

}