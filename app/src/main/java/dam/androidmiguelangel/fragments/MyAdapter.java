package dam.androidmiguelangel.fragments;

import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import dam.androidmiguelangel.fragments.R;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private int cards;
    FragmentManager FM;

    static class MyViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout fragmentContainer;
        Button btFragment1;
        Button btFragment2;
        Button btFragment3;
        FragmentManager FM;

        MyViewHolder(View itemView, FragmentManager FM) {

            super(itemView);
            RelativeLayout fragmentContainer = itemView.findViewById(R.id.fragmentContainer);
            btFragment1 = itemView.findViewById(R.id.btFragment1);
            btFragment2 = itemView.findViewById(R.id.btFragment2);
            btFragment3 = itemView.findViewById(R.id.btFragment3);
            this.FM = FM;
            Fragment1 fragment1 = new Fragment1();
            Fragment2 fragment2 = new Fragment2();
            Fragment3 fragment3 = new Fragment3();

            btFragment1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    canviaFragment(fragmentContainer, fragment1);
                }
            });
            btFragment2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    canviaFragment(fragmentContainer, fragment2);
                }
            });
            btFragment3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    canviaFragment(fragmentContainer, fragment3);
                }
            });

        }

        public void bind() {

        }

        private void canviaFragment(RelativeLayout container, Fragment fragment) {

            FragmentTransaction FT = FM.beginTransaction();

            FT.replace(container.getId(), fragment);
            FT.addToBackStack(null);

            FT.commit();

        }

    }

    MyAdapter(int cards, FragmentManager FM) {

        this.cards = cards;
        this.FM = FM;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview, parent, false);
        return new MyViewHolder(v, FM);

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
    }

    @Override
    public int getItemCount() {

        return cards;

    }

}
